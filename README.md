 # *example_of_checking_if_a_command_is_runnable*, Example of how to check if a command/application is runnable before using if (with *system* by example).

## LICENSE
**example_of_checking_if_a_command_is_runnable** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ ./linux/example_of_checking_if_a_command_is_runnable gcc
Command gcc is in path (/usr/bin/gcc).
$ ./linux/example_of_checking_if_a_command_is_runnable cobol
Command cobol is not in path or is not executable.
$ ./linux/example_of_checking_if_a_command_is_runnable Makefile
Command Makefile is not in path or is not executable.
$ ./linux/example_of_checking_if_a_command_is_runnable linux/example_of_checking_if_a_command_is_runnable
Command linux/example_of_checking_if_a_command_is_runnable is in path (linux/example_of_checking_if_a_command_is_runnable).
$ ./linux/example_of_checking_if_a_command_is_runnable /usr/bin/gcc
Command /usr/bin/gcc is in path (/usr/bin/gcc).
$ ./linux/example_of_checking_if_a_command_is_runnable
ERROR: One argument expected.

$ ./linux/example_of_checking_if_a_command_is_runnable gcc gdb
ERROR: Too many arguments supplied.

$ 
```
## STRUCTURE OF THE APPLICATION
This section walks you through **example_of_checking_if_a_command_is_runnable**'s structure. Once you understand this structure, you will easily find your way around in **example_of_checking_if_a_command_is_runnable**'s code base.

``` bash
$ yaTree
./                                                     # Application level
├── src/                                               # Source directory
│   ├── Makefile                                       # Makefile
│   └── example_of_checking_if_a_command_is_runnable.c # C Source file
├── COPYING.md                                         # GNU General Public License markdown file
├── LICENSE.md                                         # License markdown file
├── Makefile                                           # Makefile
├── README.md                                          # ReadMe markdown file
├── RELEASENOTES.md                                    # Release Notes markdown file
└── VERSION                                            # Version identification text file

1 directories, 8 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd example_of_checking_if_a_command_is_runnable
$ make clean all
```

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- Developped and tested on XUBUNTU 22.04, GCC version 11.2.0 (Ubuntu 11.2.0-19ubuntu1), GNU Make 4.3

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***