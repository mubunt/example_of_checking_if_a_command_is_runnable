# RELEASE NOTES: *example_of_checking_if_a_command_is_runnable*, How to check if a command/application is runnable..

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 1.0.1**:
  - Removed unused make file.

**Version 1.0.0**:
  - First version.
