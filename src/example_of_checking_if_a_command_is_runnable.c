//------------------------------------------------------------------------------
// Copyright (c) 2022, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: example_of_checking_if_a_command_is_runnable
// How to check if a command/application is runnable.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define error(_FMT, ...)	do { \
								fprintf(stderr, "ERROR: " _FMT "\n\n", __VA_ARGS__); \
							} while (0)
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static bool can_run_command(const char *cmd, char *realcmd) {
	if(strchr(cmd, '/')) {
		// if cmd includes a slash, no path search must be performed,
		// go straight to checking if it's executable
		strcpy(realcmd, cmd);
		return access(cmd, X_OK) == 0;
	}
	const char *path = getenv("PATH");
	if (!path) return false; // something is horribly wrong...
	// we are sure we won't need a buffer any longer
	char *buf = malloc(strlen(path)+strlen(cmd)+3);
	if (!buf) return false; // actually useless, see comment
	// loop as long as we have stuff to examine in path
	for (; *path; ++path) {
		// start from the beginning of the buffer
		char *p = buf;
		// copy in buf the current path element
		for (; *path && *path!=':'; ++path,++p) *p = *path;
		// empty path entries are treated like "."
		if (p==buf) *p++='.';
		// slash and command name
		if (p[-1]!='/') *p++='/';
		strcpy(p, cmd);
		// check if we can execute it
		if (access(buf, X_OK)==0) {
			strcpy(realcmd, buf);
			free(buf);
			return true;
		}
		// quit at last cycle
		if (!*path) break;
	}
	// not found
	free(buf);
	return false;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	//---- Parameter checking and setting --------------------------------------
	if (argc < 2) {
		error("%s", "One argument expected.");
		return EXIT_FAILURE;
	} else {
		if( argc > 2 ) {
			error("%s", "Too many arguments supplied.");
			return EXIT_FAILURE;
		}
	}
	//----  Go on --------------------------------------------------------------
	char realcmd[PATH_MAX] = "";
	if (can_run_command(argv[1], realcmd))
		fprintf(stdout, "Command %s is in path (%s).\n", argv[1], realcmd);
	else
		fprintf(stdout, "Command %s is not in path or is not executable.\n", argv[1]);
	//---- Exit ----------------------------------------------------------------
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
